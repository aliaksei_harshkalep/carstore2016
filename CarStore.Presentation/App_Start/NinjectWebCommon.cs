[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(CarStore.Presentation.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(CarStore.Presentation.App_Start.NinjectWebCommon), "Stop")]

namespace CarStore.Presentation.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Business.Repositories;
    using Business.Services;
    using Microsoft.AspNet.Identity;
    using Infrastructure.Authentication;
    using Models;
    using DataAccess.Json;
    using System.Web.Hosting;
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ICarRepository>().To<JsonCarRepository>().InRequestScope();
            kernel.Bind<IUserRepository>().To<JsonUserRepository>().InRequestScope();
            kernel.Bind<IUserRoleRepository>().To<JsonUserRoleRepository>().InRequestScope();
            kernel.Bind<ICarService>().To<CarService>().InRequestScope();

            kernel.Bind<IUserStore<UserViewModel, string>>().To<CustomUserStore>().InRequestScope();
            kernel.Bind<UserManager<UserViewModel, string>>().ToSelf().InRequestScope();

            kernel.Bind<IJsonStorage>().To<JsonStorage>().InRequestScope()
                .WithConstructorArgument("filePath", ctx => HostingEnvironment.MapPath("~/App_Data/data.json"));
        }
    }
}
