﻿using CarStore.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarStore.Presentation.Localization
{
    public static class LocalizationHelper
    {
        private static readonly LocalizationVewModel[] Localizations =
        {
            new LocalizationVewModel {LocalizationId = "en-US", Name = "English"},
            new LocalizationVewModel {LocalizationId = "ru-RU", Name = "Русский"},
        };

        public static IEnumerable<LocalizationVewModel> GetAvaliableLocalizations()
        {
            return Localizations;
        }
    }
}