﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarStore.Presentation.Models
{
    public class LocalizationVewModel
    {
        public string LocalizationId { get; set; }
        public string Name { get; set; }
    }
}