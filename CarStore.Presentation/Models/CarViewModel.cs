﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarStore.Presentation.Models
{
    public class CarViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public float Engine { get; set; }
        public int MaxSpeed { get; set; }
        public DateTime ManufactureDate { get; set; }
    }
}