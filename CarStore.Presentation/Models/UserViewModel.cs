﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarStore.Presentation.Models
{
    public class UserViewModel : IUser<string>
    {
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}