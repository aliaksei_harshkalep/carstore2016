﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarStore.Presentation.Models
{
    public class FullCarViewModel
    {
        public Guid Id { get; set; }
        public bool InStock { get; set; }
        [Required]
        public string Name { get; set; }
        [Range(0.5, 7)]
        public float Engine { get; set; }
        public int MaxSpeed { get; set; }
        [MaxLength(256)]
        public string Description { get; set; }
        public DateTime ManufactureDate { get; set; }

    }
}