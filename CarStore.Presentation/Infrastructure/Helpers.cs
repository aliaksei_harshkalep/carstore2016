﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace CarStore.Presentation.Infrastructure
{
    public static class Helpers
    {
        public static MvcHtmlString DatepickerFor<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, DateTime>> expression, object attributes)
        {
            // Get name of the property
            var name = ExpressionHelper.GetExpressionText(expression);

            // Get metadata about property (this includes attrubute data as well)
            var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            var fullHtmlFieldName = helper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);

            // Now we bild input html tag
            var tagBuilder = new TagBuilder("input");
            var attributesDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(attributes);
            tagBuilder.GenerateId(fullHtmlFieldName);
            tagBuilder.MergeAttributes(attributesDictionary);
            tagBuilder.MergeAttribute("name", fullHtmlFieldName, true);
            tagBuilder.MergeAttribute("data-provide", "datepicker");
            tagBuilder.MergeAttribute("data-date-today-btn", "true");
            tagBuilder.MergeAttribute("data-date-z-index-offset", "1200");
            tagBuilder.MergeAttribute("data-date-format", "MM/dd/yyyy"); // TODO: adopt according to localization

            // Now we have to assign proper value
            ModelState state;
            helper.ViewData.ModelState.TryGetValue(fullHtmlFieldName, out state);
            var date = state?.Value == null ? $"{metadata.Model:MM/dd/yyyy}" : state.Value.AttemptedValue;

            // Assign the value to the future input
            tagBuilder.MergeAttribute("value", date);

            // Return string for rendering
            return MvcHtmlString.Create(tagBuilder.ToString());
        }
    }
}