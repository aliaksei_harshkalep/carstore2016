﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarStore.Presentation.Infrastructure.Filters
{
    public class ExceptionLogFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            Trace.TraceError($"{DateTime.Now}: Exception in {filterContext.Controller.GetType().Name}: {filterContext.Exception.Message}");
        }
    }
}