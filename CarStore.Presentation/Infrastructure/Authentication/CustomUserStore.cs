﻿using CarStore.Presentation.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Security.Claims;
using CarStore.Business.Repositories;

namespace CarStore.Presentation.Infrastructure.Authentication
{
    public class CustomUserStore : IUserPasswordStore<UserViewModel>, IUserClaimStore<UserViewModel>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserRoleRepository _userRoleRepository;

        public CustomUserStore(IUserRepository userRepository, IUserRoleRepository userRoleRepository)
        {
            _userRepository = userRepository;
            _userRoleRepository = userRoleRepository;
        }

        public Task AddClaimAsync(UserViewModel user, Claim claim)
        {
            throw new NotImplementedException();
        }

        public Task CreateAsync(UserViewModel user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(UserViewModel user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
        }

        public Task<UserViewModel> FindByIdAsync(string userId)
        {
            var user = _userRepository.GetById(Guid.Parse(userId));
            return Task.FromResult(new UserViewModel { Id = user.Id.ToString(), UserName = user.UserName });
        }

        public Task<UserViewModel> FindByNameAsync(string userName)
        {
            var user = _userRepository.GetAll()
                .FirstOrDefault(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));

            return Task.FromResult(new UserViewModel { Id = user.Id.ToString(), UserName = user.UserName });
        }

        public Task<IList<Claim>> GetClaimsAsync(UserViewModel user)
        {
            var roles = _userRoleRepository.GetUserRoles(new Guid(user.Id));

            IList<Claim> claims = roles.Select(r => new Claim(ClaimTypes.Role, r.ToString())).ToList();
            return Task.FromResult(claims);
        }

        public Task<string> GetPasswordHashAsync(UserViewModel user)
        {
            var model = _userRepository.GetById(Guid.Parse(user.Id));
            return Task.FromResult(new PasswordHasher().HashPassword(model.Password));
        }

        public Task<bool> HasPasswordAsync(UserViewModel user)
        {
            return Task.FromResult(true);
        }

        public Task RemoveClaimAsync(UserViewModel user, Claim claim)
        {
            throw new NotImplementedException();
        }

        public Task SetPasswordHashAsync(UserViewModel user, string passwordHash)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(UserViewModel user)
        {
            throw new NotImplementedException();
        }
    }
}