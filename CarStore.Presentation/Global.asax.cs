﻿using CarStore.Business.Models;
using CarStore.Business.Repositories;
using CarStore.DataAccess.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;

namespace CarStore.Presentation
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            //InitializeData();
        }

        private void InitializeData()
        {
            var storage = new JsonStorage(HostingEnvironment.MapPath("~/App_Data/data.json"));
            var carRepo = new JsonCarRepository(storage);
            var userRepo = new JsonUserRepository(storage);
            var userRolesRepo = new JsonUserRoleRepository(storage);

            carRepo.AddCar(new Car
            {
                Id = Guid.NewGuid(),
                Name = "Aston Martin",
                InStock = false,
                Description = "James Bond car",
                MaxSpeed = 280,
                Engine = 4.5f
            });
            carRepo.AddCar(new Car
            {
                Id = Guid.NewGuid(),
                Name = "Ford",
                InStock = true,
                Description = "Just a comfortable car",
                MaxSpeed = 200,
                Engine = 2f
            });
            carRepo.AddCar(new Car
            {
                Id = Guid.NewGuid(),
                Name = "Porsche 911",
                InStock = true,
                Description = "Nice sport car",
                MaxSpeed = 250,
                Engine = 3f
            });

            userRepo.AddUser(new User
            {
                Id = new Guid("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"),
                Password = "secret",
                UserName = "Admin"
            });
            userRepo.AddUser(new User
            {
                Id = new Guid("3CA11258-2B5E-4CEC-86E8-08D0BAC8A383"),
                Password = "secret",
                UserName = "User"
            });

            userRolesRepo.AddUserToRole(Guid.Parse("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"), Role.Admin);
            userRolesRepo.AddUserToRole(Guid.Parse("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"), Role.Manager);
            userRolesRepo.AddUserToRole(Guid.Parse("3CA11258-2B5E-4CEC-86E8-08D0BAC8A383"), Role.User);

        }
    }
}
