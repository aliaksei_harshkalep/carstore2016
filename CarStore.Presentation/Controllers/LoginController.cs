﻿using CarStore.Presentation.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;

namespace CarStore.Presentation.Controllers
{
    public class LoginController : Controller
    {
        private readonly UserManager<UserViewModel, string> _userManager;

        protected IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        public LoginController(UserManager<UserViewModel, string> userManager)
        {
            _userManager = userManager;
        }

        public ActionResult Login()
        {
            return PartialView(new LoginViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginViewModel login)
        {

            var user = await _userManager.FindAsync(login.UserName, login.Password);
            if (user == null)
            {
                return View();
            }

            var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(identity);

            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Unauthorized(string returnUrl)
        {
            return View();
        }
    }
}