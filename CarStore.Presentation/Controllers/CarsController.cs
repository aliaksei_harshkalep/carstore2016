﻿using CarStore.Business.Repositories;
using CarStore.Business.Services;
using CarStore.Presentation.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarStore.Presentation.Controllers
{
    public class CarsController : Controller
    {
        private readonly ICarService _carService;
        private readonly ICarRepository _carRepository;

        public CarsController(ICarRepository carRepository, ICarService carService)
        {
            _carRepository = carRepository;
            _carService = carService;
        }

        public ActionResult Index()
        {
            var cars = _carService
                .GetAvaliableCars()
                .Select(c => new CarViewModel
                {
                    Id = c.Id,
                    Engine = c.Engine,
                    MaxSpeed = c.MaxSpeed,
                    Name = c.Name
                });

            return View(cars);
        }

        public ActionResult Details(Guid id)
        {
            var car = _carRepository.GetCarById(id);
            var vm = new FullCarViewModel
            {
                Description = car.Description,
                Engine = car.Engine,
                Id = car.Id,
                InStock = car.InStock,
                MaxSpeed = car.MaxSpeed,
                Name = car.Name
            };

            return View(vm);
        }

        public ActionResult Photo(Guid id)
        {
            var path = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Images/");
            var file = Directory.GetFiles(path, $"{id}.*").FirstOrDefault();

            if (string.IsNullOrEmpty(file)) throw new HttpException(404, "File not found");

            return File(Path.Combine(path, file), $"image/{Path.GetExtension(file)}");

        }
    }
}