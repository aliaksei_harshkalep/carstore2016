﻿using CarStore.Business.Models;
using CarStore.Business.Repositories;
using CarStore.Business.Services;
using CarStore.Presentation.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarStore.Presentation.Controllers
{
    public class AdministrationController : Controller
    {
        private readonly ICarRepository _carRepository;

        public AdministrationController(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public ActionResult Index()
        {
            var cars = _carRepository
                .GetAllCars()
                .Select(c => new CarViewModel
                {
                    Id = c.Id,
                    Engine = c.Engine,
                    MaxSpeed = c.MaxSpeed,
                    Name = c.Name
                });

            return View(cars);
        }

        public ActionResult Edit(Guid id)
        {
            var car = _carRepository.GetCarById(id);
            var vm = new FullCarViewModel
            {
                Description = car.Description,
                Engine = car.Engine,
                Id = car.Id,
                InStock = car.InStock,
                MaxSpeed = car.MaxSpeed,
                Name = car.Name,
                ManufactureDate = car.ManufactureDate
            };

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FullCarViewModel car)
        {
            if (!ModelState.IsValid) return View(car);

            SaveImage(car);
            var model = new Car
            {
                Id = car.Id,
                Description = car.Description,
                Engine = car.Engine,
                InStock = car.InStock,
                MaxSpeed = car.MaxSpeed,
                ManufactureDate = car.ManufactureDate,
                Name = car.Name
            };
            _carRepository.UpdateCar(model);

            return RedirectToAction("Index");
        }

        private void SaveImage(FullCarViewModel car)
        {
            if (Request.Files.Count == 0) return;

            var file = Request.Files[0];
            if (file.ContentLength == 0) return;

            var fileExtention = Path.GetExtension(file.FileName);

            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Images/"), $"{car.Id}.{fileExtention}");
            file.SaveAs(path);
        }
    }
}