﻿using CarStore.Business.Models;
using System.Collections;
using System.Collections.Generic;

namespace CarStore.Business.Services
{
    public interface ICarService
    {
        IEnumerable<Car> GetAvaliableCars();
    }
}
