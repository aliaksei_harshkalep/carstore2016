﻿using System;
using System.Collections.Generic;
using CarStore.Business.Models;
using CarStore.Business.Repositories;
using System.Linq;

namespace CarStore.Business.Services
{
    public class CarService : ICarService
    {
        private readonly ICarRepository _carRepository;

        public CarService(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public IEnumerable<Car> GetAvaliableCars()
        {
            var cars = _carRepository
                .GetAllCars()
                .Where(c => c.InStock)
                .ToList();

            return cars;
        }
    }
}
