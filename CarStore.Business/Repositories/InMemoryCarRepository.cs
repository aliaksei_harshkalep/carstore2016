﻿using System;
using System.Collections.Generic;
using CarStore.Business.Models;
using System.Linq;

namespace CarStore.Business.Repositories
{
    public class InMemoryCarRepository : ICarRepository
    {
        private static readonly List<Car> Cars = new List<Car>
        {
            new Car
            {
                Id = Guid.NewGuid(),
                Name = "Aston Martin",
                InStock = false,
                Description = "James Bond car",
                MaxSpeed = 280,
                Engine = 4.5f
            },
            new Car
            {
                Id = Guid.NewGuid(),
                Name = "Ford",
                InStock = true,
                Description = "Just a comfortable car",
                MaxSpeed = 200,
                Engine = 2f
            },
            new Car
            {
                Id = Guid.NewGuid(),
                Name = "Porsche 911",
                InStock = true,
                Description = "Nice sport car",
                MaxSpeed = 250,
                Engine = 3f
            },
            new Car
            {
                Id = Guid.NewGuid(),
                Name = "BMW",
                InStock = true,
                Description = "Reliable German car",
                MaxSpeed = 220,
                Engine = 4.0f
            },
            new Car
            {
                Id = Guid.NewGuid(),
                Name = "Shkoda",
                InStock = true,
                Description = "Cheep car for everybody",
                MaxSpeed = 185,
                Engine = 1.5f
            },
        };

        public void AddCar(Car model)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Car> GetAllCars()
        {
            return Cars;
        }

        public Car GetCarById(Guid id)
        {
            return Cars.FirstOrDefault(c => c.Id == id);
        }

        public void UpdateCar(Car car)
        {
            if (car == null) throw new ArgumentNullException(nameof(car));

            var existing = Cars.FirstOrDefault(c => c.Id == car.Id);
            if (existing == null) throw new InvalidOperationException();

            existing.Engine = car.Engine;
            existing.Description = car.Description;
            existing.InStock = car.InStock;
            existing.MaxSpeed = car.MaxSpeed;
            existing.Name = car.Name;
        }
    }
}
