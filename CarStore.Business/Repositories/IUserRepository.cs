﻿using CarStore.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore.Business.Repositories
{
    public interface IUserRepository
    {

        User GetById(Guid id);
        void AddUser(User user);
        IEnumerable<User> GetAll();
    }
}
