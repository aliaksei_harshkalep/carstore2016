﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStore.Business.Models;
using CarStore.DataAccess.Json;

namespace CarStore.Business.Repositories
{
    public class JsonUserRoleRepository : IUserRoleRepository
    {
        private readonly IJsonStorage _jsonStorage;

        public JsonUserRoleRepository(IJsonStorage jsonStorage)
        {
            _jsonStorage = jsonStorage;
        }

        public void AddUserToRole(Guid userId, Role role)
        {
            var user = _jsonStorage.GetById<UserJsonModel>(userId);

            var roles = new List<int>(user.Roles);
            roles.Add((int)role);
            user.Roles = roles.ToArray();

            _jsonStorage.Update(user);
            _jsonStorage.SaveChanges();
        }

        public IEnumerable<Role> GetUserRoles(Guid userId)
        {
            var user = _jsonStorage.GetById<UserJsonModel>(userId);
            return user.Roles.Select(r => (Role)r);
        }
    }
}
