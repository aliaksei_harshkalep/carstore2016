﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStore.Business.Models;

namespace CarStore.Business.Repositories
{
    public class InMemoryUserRepository : IUserRepository
    {
        private static readonly List<User> Users = new List<User>
        {
            new User
            {
                Id = new Guid("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"),
                Password = "secret",
                UserName = "Admin"
            },
            new User
            {
                Id = new Guid("3CA11258-2B5E-4CEC-86E8-08D0BAC8A383"),
                Password = "secret",
                UserName = "User"
            }
        };

        public void AddUser(User user)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetAll()
        {
            return Users;
        }

        public User GetById(Guid id)
        {
            return Users.FirstOrDefault(u => u.Id == id);
        }
    }
}
