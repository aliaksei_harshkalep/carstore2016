﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStore.Business.Models;
using CarStore.DataAccess.Json;
using Omu.ValueInjecter;

namespace CarStore.Business.Repositories
{
    public class JsonUserRepository : IUserRepository
    {
        private readonly IJsonStorage _jsonStorage;

        public JsonUserRepository(IJsonStorage jsonStorage)
        {
            _jsonStorage = jsonStorage;
        }

        public void AddUser(User user)
        {
            var jsonModel = new UserJsonModel { Id = Guid.NewGuid(), Roles = new[] { (int)Role.User } };
            jsonModel.InjectFrom(user);

            _jsonStorage.Add(jsonModel);
            _jsonStorage.SaveChanges();
        }

        public IEnumerable<User> GetAll()
        {
            return _jsonStorage.Query<UserJsonModel>().Select(u => Mapper.Map<User>(u));
        }

        public User GetById(Guid id)
        {
            return Mapper.Map<User>(_jsonStorage.GetById<UserJsonModel>(id));
        }
    }
}
