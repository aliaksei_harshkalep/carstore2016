﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStore.Business.Models;

namespace CarStore.Business.Repositories
{
    public class InMemoryUserRolesRepository : IUserRoleRepository
    {
        private static readonly List<Tuple<Guid, Role>> UserRoles = new List<Tuple<Guid, Role>>
        {
            new Tuple<Guid, Role>(new Guid("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"), Role.Admin),
            new Tuple<Guid, Role>(new Guid("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"), Role.Manager),
            new Tuple<Guid, Role>(new Guid("3CA11258-2B5E-4CEC-86E8-08D0BAC8A383"), Role.User),
        };

        public void AddUserToRole(Guid userId, Role role)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Role> GetUserRoles(Guid userId)
        {
            return UserRoles.Where(ur => ur.Item1 == userId).Select(ur => ur.Item2);
        }
    }
}
