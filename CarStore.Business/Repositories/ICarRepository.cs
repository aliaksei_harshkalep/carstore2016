﻿using CarStore.Business.Models;
using System;
using System.Collections.Generic;

namespace CarStore.Business.Repositories
{
    public interface ICarRepository
    {
        Car GetCarById(Guid id);
        IEnumerable<Car> GetAllCars();
        void AddCar(Car model);
        void UpdateCar(Car model);
    }
}
