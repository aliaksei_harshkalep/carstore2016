﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStore.Business.Models;
using CarStore.DataAccess.Json;
using Omu.ValueInjecter;

namespace CarStore.Business.Repositories
{
    public class JsonCarRepository : ICarRepository
    {
        private readonly IJsonStorage _jsonStorage;

        public JsonCarRepository(IJsonStorage jsonStorage)
        {
            _jsonStorage = jsonStorage;
        }

        public void AddCar(Car model)
        {
            var jsonModel = new CarJsonModel { Id = Guid.NewGuid() };
            jsonModel.InjectFrom(model);
            _jsonStorage.Add(jsonModel);
            _jsonStorage.SaveChanges();
        }

        public IEnumerable<Car> GetAllCars()
        {
            return _jsonStorage.Query<CarJsonModel>().Select(m => Mapper.Map<Car>(m));
        }

        public Car GetCarById(Guid id)
        {
            return Mapper.Map<Car>(_jsonStorage.GetById<CarJsonModel>(id));
        }

        public void UpdateCar(Car model)
        {
            var jsonModel = _jsonStorage.GetById<CarJsonModel>(model.Id);
            jsonModel.InjectFrom(model);
            _jsonStorage.Update(jsonModel);
            _jsonStorage.SaveChanges();
        }
    }
}
