﻿using CarStore.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore.Business.Repositories
{
    public interface IUserRoleRepository
    {
        IEnumerable<Role> GetUserRoles(Guid userId);
        void AddUserToRole(Guid userId, Role role);

    }
}
