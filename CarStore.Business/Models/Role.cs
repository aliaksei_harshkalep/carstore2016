﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore.Business.Models
{
    public enum Role
    {
        User,
        Manager,
        Admin
    }
}
