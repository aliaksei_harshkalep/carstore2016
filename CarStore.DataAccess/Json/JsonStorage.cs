﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CarStore.DataAccess.Json
{
    public class JsonStorage : IJsonStorage
    {
        private static readonly string JsonStorageMutexName = "JsonStorage";
        private static readonly JsonSerializerSettings SerializationSettings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };

        private readonly string _filePath;
        private readonly Lazy<List<JsonModelBase>> _dataProvider;

        private List<JsonModelBase> Data => _dataProvider.Value;

        public JsonStorage(string filePath)
        {
            _filePath = filePath;
            _dataProvider = new Lazy<List<JsonModelBase>>(GetData, true);
        }

        private List<JsonModelBase> GetData()
        {
            if (!File.Exists(_filePath))
            {
                using (var sw = new StreamWriter(_filePath))
                {
                    sw.Write("\n");
                }
            };

            var stringData = File.ReadAllText(_filePath);
            var data = JsonConvert.DeserializeObject<List<JsonModelBase>>(stringData, SerializationSettings);

            return data ?? new List<JsonModelBase>();
        }

        public void Add<T>(T item) where T : JsonModelBase
        {
            Data.Add(item);
        }

        public void Delete(Guid id)
        {
            Data.RemoveAll(m => m.Id == id);
        }

        public T GetById<T>(Guid id) where T : JsonModelBase
        {
            return Data.FirstOrDefault(m => m.Id == id) as T;
        }

        public IEnumerable<T> Query<T>() where T : JsonModelBase
        {
            return Data.OfType<T>();
        }

        public void SaveChanges()
        {
            var mutex = new Mutex(false, JsonStorageMutexName);
            try
            {
                using (var sw = new StreamWriter(_filePath, false))
                {
                    sw.Write(JsonConvert.SerializeObject(Data, SerializationSettings));
                }
            }
            finally
            {
                mutex.WaitOne();
            }
        }

        public void Update<T>(T item) where T : JsonModelBase
        {
            Data.RemoveAll(m => m.Id == item.Id);
            item.UpdatedOn = DateTime.Now;
            Data.Add(item);
        }
    }
}
