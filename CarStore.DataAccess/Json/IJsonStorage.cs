﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore.DataAccess.Json
{
    public interface IJsonStorage
    {
        T GetById<T>(Guid id) where T : JsonModelBase;
        IEnumerable<T> Query<T>() where T : JsonModelBase;
        void Add<T>(T item) where T : JsonModelBase;
        void Update<T>(T item) where T : JsonModelBase;
        void Delete(Guid id);
        void SaveChanges();
    }
}
