﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore.DataAccess.Json
{
    public class JsonModelBase
    {
        public Guid Id { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
