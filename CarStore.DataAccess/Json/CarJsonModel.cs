﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore.DataAccess.Json
{
    public class CarJsonModel : JsonModelBase
    {
        public bool InStock { get; set; }
        public string Name { get; set; }
        public float Engine { get; set; }
        public int MaxSpeed { get; set; }
        public string Description { get; set; }
        public DateTime ManufactureDate { get; set; }

    }
}