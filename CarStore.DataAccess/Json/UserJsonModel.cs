﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStore.DataAccess.Json
{
    public class UserJsonModel : JsonModelBase
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int[] Roles { get; set; }
    }
}
